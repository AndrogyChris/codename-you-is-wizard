using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [Header("Height Adjustor")]
    [SerializeField] Transform playerHeight;
    [SerializeField] float heightOffset;

    [Header("Ui Objects")]
    [SerializeField] GameObject startingText;
    [SerializeField] GameObject debugText;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(1f);
        transform.position = new Vector3(transform.position.x, playerHeight.position.y + heightOffset, transform.position.z);
    }

    private void OnEnable()
    {
        DrawingRecogniser.OnPause.AddListener(ShowStartingText);
    }
    private void OnDisable()
    {
        DrawingRecogniser.OnPause.RemoveListener(ShowStartingText);
    }

    private void ShowStartingText()
    {
        debugText.SetActive(false);
        startingText.SetActive(true);
    }
}

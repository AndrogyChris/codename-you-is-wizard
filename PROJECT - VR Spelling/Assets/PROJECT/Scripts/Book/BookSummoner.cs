using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookSummoner : MonoBehaviour
{
    [SerializeField] GameObject spellBook;

    [SerializeField] Transform leftHandTransform;
    [SerializeField] Transform rightHandTransform;

    [SerializeField] Transform playerTransform;

    bool leftHandReady;
    bool rightHandReady;

    public void SummonBookRight(bool active)
    {
        rightHandReady = active;
        SummonBook();
    }

    public void SummonBookLeft(bool active)
    {
        leftHandReady = active;
        SummonBook();
    }

    [ContextMenu("Summon Book")]
    private void SummonBookTest()
    {
        SummonBookRight(true);
        SummonBookLeft(true);
    }

    private void SummonBook()
    {
        if (leftHandReady && rightHandReady)
        {

            Vector3 summonLocation = (leftHandTransform.position + rightHandTransform.position) / 2;
            spellBook.transform.position = summonLocation;
            spellBook.transform.LookAt(playerTransform);
            spellBook.SetActive(true);
        }
    }
}

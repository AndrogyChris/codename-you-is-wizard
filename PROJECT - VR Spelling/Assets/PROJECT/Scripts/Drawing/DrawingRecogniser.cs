using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PDollarGestureRecognizer;
using TMPro;
using UnityEngine.Events;

[System.Serializable]
public enum SpellType
{
    Summon,
    Throw,
    Grab
}

public class DrawingRecogniser : MonoBehaviour
{
    bool isDrawing;

    [Header("Drawing")]
    [SerializeField] Transform drawingSource;
    [SerializeField] float positionThreshold;

    [Header("Spell Spawning")]
    [SerializeField] Transform spellSpawnTransform;

    [Header("Debugging")]
    [SerializeField] GameObject debugCubePrefab;
    [SerializeField] TextMeshProUGUI debugText;

    Coroutine drawingRoutine;

    private List<Gesture> trainingSet = new List<Gesture>();
    private List<Vector3> drawingPosList = new List<Vector3>();
    private List<Point> drawingPoints= new List<Point>();
    private int strokeId = -1;

    private List<GameObject> spawnedCubes = new List<GameObject>();

    public static UnityEvent<string, Transform, SpellType> OnSpellDrawnSuccess = new UnityEvent<string, Transform, SpellType>();

    public static UnityEvent OnPause = new UnityEvent();
    public static UnityEvent OnQuit = new UnityEvent();

    private void Start()
    {
        TextAsset[] gesturesXml = Resources.LoadAll<TextAsset>("GestureSet/Drawings/");
        foreach (TextAsset gestureXml in gesturesXml)
            trainingSet.Add(GestureIO.ReadGestureFromXML(gestureXml.text));
    }

    public void DrawingActivated(bool isActivated)
    {
        isDrawing = isActivated;
        if (isDrawing)
        {
            StartDrawing();
        }
        else
        {
            if (drawingRoutine != null)
            {
                StopCoroutine(drawingRoutine);
            }
        }
    }

    private void StartDrawing()
    {
        strokeId++;
        drawingPosList.Add(drawingSource.position);
        SpawnCube(drawingSource.position);
        drawingRoutine = StartCoroutine(UpdateDrawing());
    }

    IEnumerator UpdateDrawing()
    {
        while (true)
        {
            Vector3 lastPos = drawingPosList[drawingPosList.Count - 1];
            if (Vector3.Distance(drawingSource.position, lastPos) > positionThreshold)
            {
                drawingPosList.Add(drawingSource.position);
                SpawnCube(drawingSource.position);
                Vector2 screenPoint = Camera.main.WorldToScreenPoint(drawingSource.position);
                drawingPoints.Add(new Point(screenPoint.x, -screenPoint.y, strokeId));
            }
            yield return null;
        }
    }

    public void EndDrawing(int spellTypeIndex)
    {
        if (drawingPosList.Count > 0)
        {
            Point[] pointArray = drawingPoints.ToArray();

            Gesture newGesture = new Gesture(pointArray);

            Result result = PointCloudRecognizer.Classify(newGesture, trainingSet.ToArray());

            drawingPosList.Clear();
            drawingPoints.Clear();
            RemoveCubes();

            if (result.Score >= 0.85f)
            {
                if (result.GestureClass == "Time")
                {
                    OnPause.Invoke();
                }
                else
                {
                    debugText.text = $"GEST: {result.GestureClass} \n ACC: {result.Score} \n TYPE: {(SpellType)spellTypeIndex}";
                    OnSpellDrawnSuccess.Invoke(result.GestureClass, spellSpawnTransform, (SpellType)spellTypeIndex);
                }
            }
            else
            {
                debugText.text = $"GEST: {result.GestureClass} \n ACC: {result.Score} \n TYPE: Failed Cast";
            }

        }
        else
        {
            debugText.text = $"No Drawing";
        }
    }

    private void RemoveCubes()
    {
        for (int i = 0; i < spawnedCubes.Count; i++)
        {
            Destroy(spawnedCubes[i]);
        }
        spawnedCubes.Clear();
    }

    private void SpawnCube(Vector3 pos)
    {
        if (debugCubePrefab)
        {
            GameObject cube = Instantiate(debugCubePrefab, pos, Quaternion.identity);
            spawnedCubes.Add(cube);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell : MonoBehaviour
{
    [SerializeField] float spellEffectTime;

    [Header("Shooting")]
    [SerializeField] float shootVelocity;

    [Header("Summoning")]
    [SerializeField] float summonScale;
    [SerializeField] Vector3 summonOffSet;

    MeshRenderer meshRenderer;
    Rigidbody rb;
    ElementData elementData;

    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        rb = GetComponent<Rigidbody>();
    }

    public void SetSpellElement(ElementData element)
    {
        elementData = element;
        meshRenderer.material.color = elementData.elementColour;
    }

    public void StartShootSpell()
    {
        transform.SetParent(null);
        rb.useGravity = false;
        rb.velocity = transform.forward * shootVelocity;
        StartCoroutine(WaitBeforeReturnToPool());
    }

    public void StartGrabSpell()
    {
        StartCoroutine(WaitBeforeReturnToPool());
    }

    public void StartSpellSummoning()
    {
        transform.SetParent(null);
        transform.localScale *= summonScale;
        transform.localPosition += summonOffSet;
        StartCoroutine(WaitBeforeReturnToPool());
    }

    IEnumerator WaitBeforeReturnToPool()
    {
        float time = spellEffectTime;
        while (time > 0)
        {
            time -= Time.deltaTime;
            yield return null;
        }

        transform.localScale = Vector3.one * 0.2f;
        rb.velocity = Vector3.zero;
        rb.useGravity = false;
        ObjectPool_Spell.Instance.ReturnToObjectPool(this);
    }
}

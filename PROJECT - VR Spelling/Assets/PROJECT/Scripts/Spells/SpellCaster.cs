using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellCaster : MonoBehaviour
{
    [SerializeField] List<ElementData> elements = new List<ElementData>();

    private void OnEnable()
    {
        DrawingRecogniser.OnSpellDrawnSuccess.AddListener(CastSpell);
    }

    private void OnDisable()
    {
        DrawingRecogniser.OnSpellDrawnSuccess.AddListener(CastSpell);
    }

    private void CastSpell(string incommingElement, Transform spellTransform, SpellType incommingSpellType)
    {
        Spell spell = ObjectPool_Spell.Instance.GetFromObjectPool();
        spell.transform.SetParent(spellTransform);
        spell.transform.forward = spell.transform.parent.up;
        spell.transform.localPosition = Vector3.zero;

        for (int i = 0; i < elements.Count; i++)
        {
            if (elements[i].element.ToString() == incommingElement)
            {
                spell.SetSpellElement(elements[i]);
            }
        }

        switch (incommingSpellType)
        {
            case SpellType.Summon:
                spell.StartSpellSummoning();
                break;
            case SpellType.Throw:
                spell.StartShootSpell();
                break;
            case SpellType.Grab:
                spell.StartGrabSpell();
                break;
            default:
                spell.StartSpellSummoning();
                break;
        }
    }
}

public enum Elements
{
    Air,
    Earth,
    Fire,
    Water
}

[System.Serializable]
public struct ElementData
{
    public Elements element;
    public Color elementColour;

    public ElementData(Elements _element, Color _colour)
    {
        element = _element;
        elementColour = _colour;
    }
}

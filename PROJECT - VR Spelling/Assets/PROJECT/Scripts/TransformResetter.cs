using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformResetter : MonoBehaviour
{
    public void ResetTransform(bool leftHand)
    {
        transform.localPosition = Vector3.zero;
        if (leftHand)
        {
            transform.localRotation = Quaternion.identity;
        }
        else
        {
            transform.localRotation = Quaternion.Euler(0, 0, 180);
        }

    }
}
